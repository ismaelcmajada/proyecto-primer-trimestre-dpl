let express = require('express');

let router = express.Router();

//Controlador

let promotionControllerAPI= require("../../controllers/api/promotionControllerAPI");

//Rutas

//Get

router.get("/", promotionControllerAPI.promotion_list);
router.get("/:_id", promotionControllerAPI.promotion_list_one);

//Post

router.post("/create", promotionControllerAPI.promotion_create);

//Delete

router.delete("/delete", promotionControllerAPI.promotion_delete);

//Put

router.put("/update/:_id", promotionControllerAPI.promotion_update);

//Exportación del router

module.exports = router;