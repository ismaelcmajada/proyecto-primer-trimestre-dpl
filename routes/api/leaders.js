let express = require('express');

let router = express.Router();

//Controlador

let leaderControllerAPI= require("../../controllers/api/leaderControllerAPI");

//Rutas

//Get

router.get("/", leaderControllerAPI.leader_list);
router.get("/:_id", leaderControllerAPI.leader_list_one);

//Post

router.post("/create", leaderControllerAPI.leader_create);

//Delete

router.delete("/delete", leaderControllerAPI.leader_delete);

//Put

router.put("/update/:_id", leaderControllerAPI.leader_update);

//Exportación del router

module.exports = router;