let express = require('express');

let router = express.Router();

//Controlador

let dishControllerAPI= require("../../controllers/api/dishControllerAPI");

//Rutas

//Get

router.get("/", dishControllerAPI.dish_list);
router.get("/:_id", dishControllerAPI.dish_list_one);

//Post

router.post("/create", dishControllerAPI.dish_create);
router.post("/comment/:_id", dishControllerAPI.dish_create_comment);

//Delete

router.delete("/delete", dishControllerAPI.dish_delete);
router.delete("/comment/:_id", dishControllerAPI.dish_delete_comment);

//Put

router.put("/update/:_id", dishControllerAPI.dish_update);
router.put("/comment/:_id", dishControllerAPI.dish_update_comment);

//Exportación del router

module.exports = router;