let Leader = require("../../models/Leader");

//Devuelve todos los líderes.
exports.leader_list = function(req, res) {

    Leader.allLeaders(function (err, leaders) {
        if (err) res.send(500, err.message);

        res.status(200).json ({
            leaders: leaders
    
        });
       
      })
    
}

//Devuelve el líder con id pasado por parámetro.
exports.leader_list_one = function(req, res) {

    Leader.findById(req.params._id, function (err, leader) {
        
        if (err) res.send(500, err.message);

        res.status(200).json ({
            leaders: leader
    
        });
       
      })
    
}

//Crea un líder con los datos pasados por body
exports.leader_create = function(req, res) {

    let leader = new Leader({
        name: req.body.name,
        image: req.body.image,
        designation: req.body.designation,
        abbr: req.body.abbr,
        featured: req.body.featured,
        description: req.body.description
    });
    
    Leader.add(leader, function (err,newLeader) {
    
    if (err) res.send(500, err.message);
    
        res.status(201).json({
            leader: newLeader
        });
      });
  }

//Borra el líder con el id pasado por parámetro.
exports.leader_delete = function(req, res) {

    Leader.removeById(req.body._id, function (err) {
        if (err) res.send(500, err.message);
        res.status(204).send();
    });
}

//Actualiza el líder con id pasado por parámetro, usando los datos pasados por body.
exports.leader_update = function(req, res) {

    Leader.findById(req.params._id, function (err, leader) {
        if (err) res.send(500, err.message);
    
        leader.name = req.body.name;
        leader.image = req.body.image;
        leader.designation = req.body.designation;
        leader.abbr = req.body.abbr;
        leader.featured = req.body.featured;
        leader.description = req.body.description;
    
        leader.save();
    
        res.status(200).json({
            leader: leader
        });
    });
}