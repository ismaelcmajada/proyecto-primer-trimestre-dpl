let Promotion = require("../../models/Promotion");


//Devuelve todas las promociones.
exports.promotion_list = function(req, res) {

    Promotion.allPromotions(function (err, promotions) {
        if (err) res.send(500, err.message);

        res.status(200).json ({
            promotions: promotions
    
        });
       
      })
    
}

//Devuelve la promoción con id pasado por parámetro.
exports.promotion_list_one = function(req, res) {

    Promotion.findById(req.params._id, function (err, promotion) {
        
        if (err) res.send(500, err.message);

        res.status(200).json ({
            promotions: promotion
    
        });
       
      })
    
}

//Crea una nueva promoción con los datos pasados por body.
exports.promotion_create = function(req, res) {

    let promotion = new Promotion({
        name: req.body.name,
        image: req.body.image,
        label: req.body.label,
        price: req.body.price,
        featured: req.body.featured,
        description: req.body.description
    });
    
    Promotion.add(promotion, function (err,newPromotion) {
    
    if (err) res.send(500, err.message);
    
        res.status(201).json({
            promotion: newPromotion
        });
      });
  }

//Elimina la promoción con id pasado por body.
exports.promotion_delete = function(req, res) {

    Promotion.removeById(req.body._id, function (err) {
        if (err) res.send(500, err.message);
        res.status(204).send();
    });
}

exports.promotion_update = function(req, res) {

    Promotion.findById(req.params._id, function (err, promotion) {
        if (err) res.send(500, err.message);
    
        promotion.name = req.body.name;
        promotion.image = req.body.image;
        promotion.label = req.body.label;
        promotion.price = req.body.price;
        promotion.featured = req.body.featured;
        promotion.description = req.body.description;
    
        promotion.save();
    
        res.status(200).json({
            promotion: promotion
        });
    });
}