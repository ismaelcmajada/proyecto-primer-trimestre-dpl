let Dish = require("../../models/Dish");


//Devuelve todos los platos, usando la función allDishes del modelo.
exports.dish_list = function(req, res) {

    Dish.allDishes(function (err, dishes) {
        if (err) res.send(500, err.message);

        res.status(200).json ({
            dishes: dishes
    
        });
       
      })
    
}

//Devuelve el plato con el id pasado por parámetro.
exports.dish_list_one = function(req, res) {

    Dish.findById(req.params._id, function (err, dish) {
        
        if (err) res.send(500, err.message);

        res.status(200).json ({
            dishes: dish
    
        });
       
      })
    
}

//Crea un nuevo plato con los datos pasados por body.
exports.dish_create = function(req, res) {

    let dish = new Dish({
        name: req.body.name,
        image: req.body.image,
        category: req.body.category,
        label: req.body.label,
        price: req.body.price,
        featured: req.body.featured,
        description: req.body.description,
    });
    
    Dish.add(dish, function (err,newDish) {
    
    if (err) res.send(500, err.message);
    
        res.status(201).json({
            dish: newDish
        });
      });
  }

//Añade un comentario al array del plato.
exports.dish_create_comment = function(req, res) {
    Dish.findByIdAndUpdate(req.params._id, {
        $push: 
        {
            comments:
            [
                {
                    rating: req.body.rating,
                    comment: req.body.comment,
                    author: req.body.author,
                    date: req.body.date
                }
            ]
        }
    }, function (err){
        if(err) res.send(500, err.message);

        res.status(201).send();
    
    });
}

//Actualiza el comentario con el id pasado por parámetro.
exports.dish_update_comment = function(req, res) {
    Dish.update({"comments._id": req.params._id}, {
        $set:
        {
            "comments.$.rating": req.body.rating,
            "comments.$.comment": req.body.comment,
            "comments.$.author": req.body.author,
            "comments.$.date": req.body.date,
        }

    }, function (err) {
        if(err) res.status(500).send(err.message);

        res.status(200).send();
    });
   
}

//Elimina el comentario con el id pasado por body del plato con id pasado por parámetro.
exports.dish_delete_comment = function(req, res) {
    Dish.findByIdAndUpdate(req.params._id, {
        $pull: 
        {
            comments: { _id : req.body._id}
        }
    }, function (err){
        if(err) res.send(500, err.message);

        res.status(204).send();
    
    });
}

//Elimina el plato con id pasado por body.
exports.dish_delete = function(req, res) {

    Dish.removeById(req.body._id, function (err) {
        if (err) res.send(500, err.message);
        res.status(204).send();
    });
}

//Actualiza el plato con id pasado por parámetro usando los datos pasados por body.
exports.dish_update = function(req, res) {

    Dish.findById(req.params._id, function (err, dish) {
        if (err) res.send(500, err.message);
    
        dish.name = req.body.name,
        dish.image = req.body.image,
        dish.category = req.body.category,
        dish.label = req.body.label,
        dish.price = req.body.price,
        dish.featured = req.body.featured,
        dish.description = req.body.description,
    
        dish.save();
    
        res.status(200).json({
         dish: dish
        });
    });
}