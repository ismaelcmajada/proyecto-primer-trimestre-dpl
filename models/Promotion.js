let mongoose = require("mongoose");
let Schema = mongoose.Schema;

//Esquema promotion

let promotionSchema = new Schema ({

    name: String,
    image: String,
    label: String,
    price: Number,
    featured: Boolean,
    description: String
  
});

//Métodos del esquema

promotionSchema.statics.allPromotions = function (cb) {
    return this.find({}, cb);
}
      
promotionSchema.statics.add = function (promotion, cb) {
    return this.create(promotion, cb);
}
      
promotionSchema.statics.findById = function (promotion_id, cb) {
    return this.findOne({_id: promotion_id}, cb);
}
      
promotionSchema.statics.removeById = function (promotion_id, cb) {
    return this.deleteOne({_id: promotion_id}, cb);
}

//Exportación del modelo.

module.exports = mongoose.model("Promotion", promotionSchema);