let mongoose = require("mongoose");
let Schema = mongoose.Schema;

//Esquema leader

let leaderSchema = new Schema ({

    name: String,
    image: String,
    designation: String,
    abbr: String,
    featured: Boolean,
    description: String

});

//Métodos del esquema

leaderSchema.statics.allLeaders = function (cb) {
    return this.find({}, cb);
}
      
leaderSchema.statics.add = function (leader, cb) {
    return this.create(leader, cb);
}
      
leaderSchema.statics.findById = function (leader_id, cb) {
    return this.findOne({_id: leader_id}, cb);
}
      
leaderSchema.statics.removeById = function (leader_id, cb) {
    return this.deleteOne({_id: leader_id}, cb);
}

//Exportación del modelo.

module.exports = mongoose.model("Leader", leaderSchema);