let mongoose = require("mongoose");
let Schema = mongoose.Schema;

//Esquema dish

let dishSchema = new Schema ({

    name: String,
    image: String,
    category: String,
    label: String,
    price: Number,
    featured: Boolean,
    description: String,
    comments: [{
        rating: Number,
        comment: String,
        author: String,
        date: Date
    }]
});

//Métodos del esquema

dishSchema.statics.allDishes = function (cb) {
    return this.find({}, cb);
}
      
dishSchema.statics.add = function (dish, cb) {
    return this.create(dish, cb);
}
      
dishSchema.statics.findById = function (dish_id, cb) {
    return this.findOne({_id: dish_id}, cb);
}
      
dishSchema.statics.removeById = function (dish_id, cb) {
    return this.deleteOne({_id: dish_id}, cb);
}

//Exportación del modelo.

module.exports = mongoose.model("Dish", dishSchema);